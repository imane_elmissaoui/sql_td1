
------------------------------------------------------------------ 
-- select
------------------------------------------------------------------ 

select * from patient;

select id_patient, sexe from patient;

select count(*) from patient;

-- Q1 Afficher la table séjour 
select * 
from sejour;


-- where 
------------------------------------------------------------------ 

select *
from patient 
where sexe = 'M';

select *
from patient
where date_naissance > '1960-01-01';

-- Q2 Sélectionner les patients de la ville 1
select *
from patient
where id_ville = 1;

-- Q3 Afficher les patients nés après le 31/03/1986
select *
from patient 
where date_naissance > '1986-03-31';

-- AND
----------------------------------------------1/-------------------- 

select *
from patient
where date_naissance > '1960-01-01'
and sexe = 'F';

-- Q4 Afficher les séjours commencés après le 01/02/2020 dans l’hôpital 1
select *
from sejour
where date_debut_sejour > '2020-02-01' AND id_hopital = 1;

-- IN
------------------------------------------------------------------ 

select * 
from patient
where id_ville in (1, 2);

-- Q5 Afficher les séjours des hôpitaux 1 et 3
select *
from sejour
where id_hopital in (1,3);

-- GROUP BY
------------------------------------------------------------------ 

select sexe, count(*)
from patient
group by sexe;

-- Q6 Compter le nombre de patient par ville
select id_ville , count(*)
from patient
group by id_ville;  


-- Q7 Compter le nombre de séjours par hopital
select id_hopital , count(*)
from sejour
group by id_hopital;

-- INNER JOIN
------------------------------------------------------------------ 

select *
from patient p inner join ville v 
on p.id_ville = v.id_ville;

-- Q8 Modifier la requête précédente pour n'afficher que l'id_patient et le nom de la ville

select id_patient, ville
from patient p inner join ville v 
on p.id_ville = v.id_ville;

-- Q9 Afficher, pour chaque séjour, les hôptiaux dans lesquels ils ont eu lieu
select id_sejour, hopital
from sejour s inner join hopital h
on s.id_hopital = h.id_hopital;



-- Q10 Compter le nombre de patients par ville en affichant le NOM de la ville
select ville, count(p.id_patient) as nombre_de_patients
from patient p inner join ville v 
on p.id_ville = v.id_ville 
group by ville;


-- Q11 Compter le nombre de séjours par hôpital en affichant le NOM de l'hôpital
select hopital, count(s.id_sejour) as nombre_de_sejours
from sejour s inner join hopital h
on s.id_hopital = h.id_hopital
group by hopital;



-- Q12 Compter le nombre de patients femme par ville en affichant le nom de la ville
select ville, count(p.sexe) as nombre_de_patients_femmes
from patient p inner join ville v 
on p.id_ville = v.id_ville
where sexe = 'F'
group by ville;

-- Q13 Compter le nombre de séjours commençés après le 01/02/2020 pour chaque hôpital en affichant le nom de l'hôpital
select hopital, count(s.id_sejour) as nombre_de_sejours
from sejour s inner join hopital h
on s.id_hopital = h.id_hopital
where date_debut_sejour > '2020-02-01'
group by hopital;





-- insert
------------------------------------------------------------------ 

-- Exécuter la requête et **interpréter** le résultat :

INSERT INTO ville          
(id_ville, ville)
VALUES(6, 'Béthune');

--Intérprétation : on ajoute dans la table ville une nouvelle ligne tel que l'identifiant est 6 et la ville correspondante est Béthune

-- Q13 Insérer Loos dans la table ville
INSERT INTO ville 
(id_ville,ville)
VALUES(7,'Loos');

-- Intérprétation : on ajoute dans la table ville une nouvelle ligne tel que l'identifiant est 7 et la ville correspondante est Loos

-- update
------------------------------------------------------------------ 

-- Exécuter la requête et **interpréter** le résultat :

update ville set ville = 'Lens' where id_ville = 6;

-- Intérprétation : On remplace le nom de la ville qui correspond à id_ville = 6(Béthune) par Lens (Mise à jour des données)

-- Q14 Remplacer le libellé de la ville numéro 7 par Douai

update ville set ville = 'Douai' where id_ville = 7;

-- Intérprétation : On remplace le nom de la ville qui correspond à id_ville = 7(Loos) par DOuai

-- delete
------------------------------------------------------------------ 

-- Exécuter la requête et **interpréter** le résultat :

delete from ville where id_ville = 6;
-- On supprime toute la ligne qui correspond à id_ville = 6 et ville = Lens 

-- Q15 supprimer la ville numéro 7
delete from ville where id_ville = 7;
--  On supprime toute la ligne qui correspond à id_ville = 7 et ville = Douai

